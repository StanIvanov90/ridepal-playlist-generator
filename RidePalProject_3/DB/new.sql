use ridepaldb;

CREATE TABLE `users`
(
    `username` varchar(50) NOT NULL,
    `password` varchar(68) NOT NULL,
    `enabled`  tinyint(4)  NOT NULL,
    PRIMARY KEY (`username`)
);
INSERT INTO users (`username`, `password`, `enabled`)
VALUES ('pesho', '{noop}pass1', 1),
       ('nadya', '{noop}pass2', 1),
       ('misho', '{noop}pass3', 1);
CREATE TABLE `authorities`
(
    `username`  varchar(50) NOT NULL,
    `authority` varchar(50) NOT NULL,
    UNIQUE KEY `username_authority` (`username`, `authority`),
    CONSTRAINT `authorities_fk` FOREIGN KEY (`username`) REFERENCES users (`username`)
);
INSERT INTO `authorities` (`username`, `authority`)
VALUES ('pesho', 'ROLE_USER');


