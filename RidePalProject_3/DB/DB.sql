USE RidePalDB;

create table artists
(
    artist_id   int          not null
        primary key,
    artist_name varchar(255) not null,
    picture     mediumtext   null
);

create table genres
(
    genre_id   int          not null
        primary key,
    genre_name varchar(255) not null,
    picture    varchar(200) null
);

create table albums
(
    album_id  int          not null
        primary key,
    title     varchar(255) not null,
    cover     mediumtext   null,
    duration  int          not null,
    preview   varchar(255) null,
    tracklist varchar(255) null,
    genre_id  int(255)     null,
    constraint albums_genres_genre_id_fk
        foreign key (genre_id) references genres (genre_id)
);


create table album_genres
(
    album_genre_id int auto_increment
        primary key,
    album_id       int not null,
    genre_id       int not null,
    constraint album_genres_ibfk_1
        foreign key (album_id) references albums (album_id),
    constraint album_genres_ibfk_2
        foreign key (genre_id) references genres (genre_id)
);

create index album_id
    on album_genres (album_id);

create index genre_id
    on album_genres (genre_id);

create table roles
(
    role_id int auto_increment
        primary key,
    role    varchar(20) null
);

create table tracks
(
    track_id  bigint(11)           not null
        primary key,
    title     varchar(255)         not null,
    artist_id int                  null,
    album_id  int                  null,
    duration  int                  not null,
    `rank`    int                  not null,
    preview   varchar(200)         null,
    enabled   tinyint(1) default 1 not null,
    constraint FKdcmijveo7n1lql01vav1u2jd2
        foreign key (album_id) references albums (album_id),
    constraint FKkiacd31n79ksp3mnq6owsbjiu
        foreign key (artist_id) references artists (artist_id)
);

create table users
(
    username varchar(50) not null
        primary key,
    password varchar(68) not null,
    enabled  tinyint     not null
);

create table authorities
(
    username  varchar(50) not null,
    authority varchar(50) not null,
    constraint username_authority
        unique (username, authority),
    constraint authorities_fk
        foreign key (username) references users (username)
);

create table users_details
(
    user_id    int auto_increment
        primary key,
    username   varchar(20)          not null,
    first_name varchar(50)          not null,
    last_name  varchar(50)          not null,
    email      varchar(50)          not null,
    avatar     mediumblob            null,
    enabled    tinyint(1) default 1 not null,
    constraint email
        unique (email),
    constraint username
        unique (username)
);

create table playlists
(
    playlist_id int auto_increment
        primary key,
    title       varchar(20)          not null,
    description varchar(200)         null,
    duration    int                  not null,
    `rank`      int                  not null,
    user_id     int                  not null,
    cover       varchar(200)         null,
    enabled     tinyint(1) default 1 null,
    constraint playlists_ibfk_1
        foreign key (user_id) references users_details (user_id)
);

create table playlist_genres
(
    playlist_genre_id int auto_increment
        primary key,
    playlist_id       int not null,
    genre_id          int not null,
    constraint playlist_genres_ibfk_1
        foreign key (playlist_id) references playlists (playlist_id),
    constraint playlist_genres_ibfk_2
        foreign key (genre_id) references genres (genre_id)
);

create index genre_id
    on playlist_genres (genre_id);

create index playlist_id
    on playlist_genres (playlist_id);

create index user_id
    on playlists (user_id);

create table user_roles
(
    user_role_id int auto_increment
        primary key,
    user_id      int not null,
    role_id      int not null,
    constraint user_roles_ibfk_1
        foreign key (user_id) references users_details (user_id),
    constraint user_roles_ibfk_2
        foreign key (role_id) references roles (role_id)
);

create index role_id
    on user_roles (role_id);

create index user_id
    on user_roles (user_id);


