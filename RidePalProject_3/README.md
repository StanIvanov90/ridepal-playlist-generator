## **RidePal – Playlist Generator**

Trello: https://trello.com/b/eAXH0eAn/a-16-team-02-ridepal

RidePal Playlist Generator is a web application. RidePal Playlist Generator enables users to generate playlists for specific travel duration periods based on their preferred genres.
A playlist consists of a list of individual tracks (songs). Each track has an artist, title, album, duration (playtime length) and rank (a numeric value). Tracks have a preview URL to an audio stream (e.g. the users can click and play the preview part of the track).
Each playlist has a user given title, associated tags (e.g. musical genres), a list of tracks with the associated track details, total playtime (the sum of the playtimes of all tracks in that playlist) and rank (the average of the ranks of all tracks in that playlist).


### How to use RidePal

| Mapping | Description |
| ------ | ------ |
| / | Home page |
| /login | Login page |
| /register | Register page |
| /playlists| Playlists|
| /playlist/{id} | Listen playlist's track previews |
|/newplaylist| Create new playlis|
| /user/{id} | Profile page |
|/users/{id}/createdplaylists| User's created playlists|
| /users/update/1 | Update profile page |
|/users | Admin can view all users|
| /genres | Admin can see all genres|


### Screenshots

- Home
![home](/RidePalProject_3/Screenshots/home.png)

- Login
![login](/RidePalProject_3/Screenshots/login.png)

- Profile
![user](/RidePalProject_3/Screenshots/user.png)

- Playlists
![playlists](/RidePalProject_3/Screenshots/playlists.png)

