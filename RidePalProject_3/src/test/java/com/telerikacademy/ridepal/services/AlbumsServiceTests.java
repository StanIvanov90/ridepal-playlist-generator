package com.telerikacademy.ridepal.services;

import com.telerikacademy.ridepal.exceptions.EntityNotFoundException;
import com.telerikacademy.ridepal.repositories.AlbumsRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.anyInt;

@RunWith(MockitoJUnitRunner.class)

public class AlbumsServiceTests {

    @Mock
    AlbumsRepository albumsRepository;

    @InjectMocks
    AlbumsService albumsService;

    @Test
    public void getAllShould_CallRepository() {
        //Arrange

        //Act
        albumsService.getAll();

        //Assert
        Mockito.verify(albumsRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getByIdShould_CallRepository(){
        //Arrange
        Mockito.when(albumsRepository.existsById(anyInt())).thenReturn(true);
        //Act
        albumsService.getById(anyInt());
        //Assert
        Mockito.verify(albumsRepository, Mockito.times(1)).getAlbumById(anyInt());
    }
    @Test(expected = EntityNotFoundException.class)
    public void getByIdShould_ThrowEntityNotFound(){
        //Act
        albumsService.getById(anyInt());
    }
}
