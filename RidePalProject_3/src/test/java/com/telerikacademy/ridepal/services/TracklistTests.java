package com.telerikacademy.ridepal.services;

import com.telerikacademy.ridepal.exceptions.EntityNotFoundException;
import com.telerikacademy.ridepal.repositories.TracksRepository;
import com.telerikacademy.ridepal.services.TracksService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.anyInt;

@RunWith(MockitoJUnitRunner.class)

public class TracklistTests {

    @Mock
    TracksRepository tracksRepository;
    @InjectMocks
    TracksService tracksService;


    @Test
    public void trackListShould_CallRepositoryWhenAllTrue() {
        //Arrange

        String genre = "Metal";
        int duration = 60;
        //Act
        tracksService.trackList(genre,duration,true,true);

        //Assert
        Mockito.verify(tracksRepository,Mockito.times(1)).findTop(genre);
    }
    @Test
    public void trackListShould_CallRepositoryWhenOnlyTopTrackIsTrue() {
        //Arrange

        String genre = "Metal";
        int duration = 60;
        //Act
        tracksService.trackList(genre,duration,true,false);

        //Assert
        Mockito.verify(tracksRepository,Mockito.times(1)).findTopByRankDistinct(genre);
    }
    @Test
    public void trackListShould_CallRepositoryWhenOnlyUseArtistsIsTrue() {
        //Arrange

        String genre = "Metal";
        int duration = 60;
        //Act
        tracksService.trackList(genre,duration,false,true);

        //Assert
        Mockito.verify(tracksRepository,Mockito.times(1)).findtracks(genre);
    }
    @Test
    public void trackListShould_CallRepositoryWhenAllFalse() {
        //Arrange

        String genre = "Metal";
        int duration = 60;
        //Act
        tracksService.trackList(genre,duration,false,false);

        //Assert
        Mockito.verify(tracksRepository,Mockito.times(1)).findtracksDistinctArtist(genre);
    }

    @Test
    public void getAllShould_CallRepository() {

        //Act
        tracksService.getAll();
        //Assert
        Mockito.verify(tracksRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getByIdShould_CallRepository(){
        //Arrange
        Mockito.when(tracksRepository.existsById(anyInt())).thenReturn(true);
        //Act
        tracksService.getById(anyInt());
        //Assert
        Mockito.verify(tracksRepository, Mockito.times(1)).getTrackById(anyInt());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getByIdShould_ThrowEntityNotFound(){
        //Act
        tracksService.getById(anyInt());
    }

    @Test
    public void getTrackListByIdShould_CallRepository(){
        //Act
        tracksService.getTracksByPlaylistId(anyInt());
        //Assert
        Mockito.verify(tracksRepository,Mockito.times(1)).getTracksByPlaylistId(anyInt());
    }


}

