package com.telerikacademy.ridepal.services;


import com.telerikacademy.ridepal.Factory;
import com.telerikacademy.ridepal.exceptions.EntityNotFoundException;
import com.telerikacademy.ridepal.models.*;
import com.telerikacademy.ridepal.repositories.GenresRepository;
import com.telerikacademy.ridepal.repositories.PlaylistsRepository;
import com.telerikacademy.ridepal.repositories.TracksRepository;
import com.telerikacademy.ridepal.services.PlaylistsService;
import com.telerikacademy.ridepal.services.TracksService;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.ArgumentMatchers.*;

@RunWith(MockitoJUnitRunner.class)

public class PlaylistsServiceTests {

    @Mock
    PlaylistsRepository playlistsRepository;
    @Mock
    GenresRepository genresRepository;
    @Mock
    DtoMapper dtoMapper;
    @Mock
    TracksRepository tracksRepository;

    @Mock
    TracksService tracksService;

    @InjectMocks
    PlaylistsService playlistsService;



    @Test
    public void getAllShould_CallRepository() {
        //Arrange

        //Act
        playlistsService.getAll();

        //Assert
        Mockito.verify(playlistsRepository, Mockito.times(1)).getAll();
    }
    @Test
    public void filterByNameShould_CallRepository() {
        //Arrange
        List<Playlist> test = new ArrayList<>();
        Playlist newPlaylist = new Playlist();
        test.add(newPlaylist);
        String testString = "test";
        Mockito.when(playlistsRepository.findAllByTitleContainingAndEnabledTrue(testString))
                .thenReturn(test);
        //Act
        playlistsService.filterByName(test,testString);

        //Assert
        Mockito.verify(playlistsRepository, Mockito.times(2)).findAllByTitleContainingAndEnabledTrue(testString);
    }

    @Test
    public void filterByNameAndUserShould_CallRepository() {
        //Arrange
        List<Playlist> test = new ArrayList<>();
        Playlist newPlaylist = new Playlist();
        test.add(newPlaylist);
        String testString = "test";
        UserDetails expectedUser = Factory.createUser();
        Mockito.when(playlistsRepository.findAllByTitleContainingAndEnabledTrueAndAndUserDetails(testString, expectedUser ))
                .thenReturn(test);
        //Act
        playlistsService.filterByNameAndUser(test,testString, expectedUser);

        //Assert
        Mockito.verify(playlistsRepository, Mockito.times(2)).findAllByTitleContainingAndEnabledTrueAndAndUserDetails(testString, expectedUser);
    }


    @Test
    public void filterByNameShould_ThrowEntityNotFound() {
        //Arrange
        List<Playlist> test = new ArrayList<>();
        String testString = "test";
        UserDetails expectedUser = Factory.createUser();
        Mockito.when(playlistsRepository.findAllByTitleContainingAndEnabledTrueAndAndUserDetails(testString, expectedUser))
                .thenReturn(test);
        //Act,Arrange
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> playlistsService.filterByNameAndUser(test,testString, expectedUser));
    }

    @Test
    public void filterByNameAndUserShould_ThrowEntityNotFound() {
        //Arrange
        List<Playlist> test = new ArrayList<>();
        String testString = "test";
        UserDetails expectedUser = Factory.createUser();
        Mockito.when(playlistsRepository.findAllByTitleContainingAndEnabledTrueAndAndUserDetails(testString, expectedUser))
                .thenReturn(test);
        //Act,Arrange
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> playlistsService.filterByNameAndUser(test,testString, expectedUser));
    }

    @Test
    public void filterByDurationShould_CallRepository() {
        //Arrange
        List<Playlist> test = new ArrayList<>();
        Playlist newPlaylist = new Playlist();
        test.add(newPlaylist);
        String testString = "test";
        UserDetails expectedUser = Factory.createUser();
        //Act
        playlistsService.filterByDurationAndUser(test,1,2, expectedUser);

        //Assert
        Mockito.verify(playlistsRepository, Mockito.times(1)).filterByDurationAndUserDetails(1*60*60,2*60*60,expectedUser.getId(), test);
    }

    @Test
    public void filterByDurationAndUserShould_CallRepository() {
        //Arrange
        List<Playlist> test = new ArrayList<>();
        Playlist newPlaylist = new Playlist();
        test.add(newPlaylist);
        String testString = "test";
        UserDetails expectedUser = Factory.createUser();
        expectedUser.setId(1);
        //Act
        playlistsService.filterByDurationAndUser(test,1,2, expectedUser);

        //Assert
        Mockito.verify(playlistsRepository, Mockito.times(1)).filterByDurationAndUserDetails(1*60*60,2*60*60,expectedUser.getId()
                ,test);
    }

    @Test
    public void filterByGenreShould_CallRepository() {
        List<Playlist> test = new ArrayList<>();
        Playlist newPlaylist = new Playlist();
        test.add(newPlaylist);
        String testString = "test";
        Mockito.when(playlistsRepository.filterByGenre(testString))
                .thenReturn(test);
        //Act
        playlistsService.filterByGenre(test,testString);

        //Assert
        Mockito.verify(playlistsRepository, Mockito.times(2)).filterByGenre(testString);
    }

    @Test
    public void filterByGenreAndUserShould_CallRepository() {
        List<Playlist> test = new ArrayList<>();
        Playlist newPlaylist = new Playlist();
        test.add(newPlaylist);
        String testString = "test";
        UserDetails expectedUser = Factory.createUser();
        Mockito.when(playlistsRepository.filterByGenreAndUser(testString, expectedUser.getId(), test))
                .thenReturn(test);
        //Act
        playlistsService.filterByGenreAndUser(test,testString, expectedUser);

        //Assert
        Mockito.verify(playlistsRepository, Mockito.times(2)).filterByGenreAndUser(testString, expectedUser.getId(),test);
    }
    @Test
    public void filterByGenreShould_ThrowEntityNotFound() {
        //Arrange
        List<Playlist> test = new ArrayList<>();
        String testString = "test";
        Mockito.when(playlistsRepository.filterByGenre(testString))
                .thenReturn(test);
        //Act,Arrange
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> playlistsService.filterByGenre(test,testString));
    }

    @Test
    public void filterByGenreAndUserShould_ThrowEntityNotFound() {
        //Arrange
        List<Playlist> test = new ArrayList<>();
        String testString = "test";
        UserDetails expectedUser = Factory.createUser();

        Mockito.when(playlistsRepository.filterByGenreAndUser(testString, expectedUser.getId(), test))
                .thenReturn(test);
        //Act,Arrange
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> playlistsService.filterByGenreAndUser(test,testString,expectedUser ));
    }

    @Test
    public void getByIdShould_CallRepository() {
        //Arrange
        Playlist playlist = new Playlist();
        Mockito.when(playlistsRepository.existsById(anyInt())).thenReturn(true);
        Mockito.when(playlistsRepository.getById(anyInt()))
                .thenReturn(playlist);
        //Act
        playlistsService.getById(1);

        //Assert
        Mockito.verify(playlistsRepository, Mockito.times(1)).getById(anyInt());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getByIdShould_ThrowEntityNotFound() {

        //Act
        playlistsService.getById(anyInt());

    }


    @Test
    public void updateShould_CallRepository() {
        Playlist playlist = new Playlist();
        Mockito.when(playlistsRepository.existsById(anyInt())).thenReturn(true);

        //Act
        playlistsService.updatePlaylist(playlist);

        //Assert
        Mockito.verify(playlistsRepository, Mockito.times(1)).save(playlist);
    }


    @Test(expected = EntityNotFoundException.class)
    public void updateShould_ThrowEntityNotFound() {

        //Arrange
        Playlist playlist = new Playlist();

        //Act
        playlistsService.updatePlaylist(playlist);
    }



    @Test
    public void deleteShould_CallRepository() {
        Playlist playlist = new Playlist();
        playlist.setId(1);
        Mockito.when(playlistsRepository.existsById(anyInt())).thenReturn(true);
        Mockito.when(playlistsRepository.getById(anyInt())).thenReturn(playlist);


        //Act
        playlistsService.deletePlaylist(anyInt());

        //Assert
        Mockito.verify(playlistsRepository, Mockito.times(1)).save(playlist);
    }

    @Test(expected = EntityNotFoundException.class)
    public void deleteShould_ThrowEntityNotFound() {

        //Arrange
        Playlist playlist = new Playlist();

        //Act
        playlistsService.deletePlaylist(playlist.getId());
    }
    @Test
    public void getPlaylistByUserShould_CallRepository() {
        //Arrange

        //Act
        playlistsService.getPlaylistsByUser(anyInt());

        //Assert
        Mockito.verify(playlistsRepository, Mockito.times(1)).getPlaylistsByUser(anyInt());
    }




    @Test
    public void createPlaylistShould_CallRepository() {
        //Arrange
        PlaylistDto playlistDto = new PlaylistDto();
        playlistDto.setTitle("test");
        playlistDto.setDescription("test");
        Playlist playlist = new Playlist();
        Genre newGenre = genresRepository.findByName("Metal");
        List<String> time = new ArrayList<>();
        time.add("60");
        time.add("40");
        playlistDto.setTime(time);
        List<String> genre = new ArrayList<>();
        genre.add("152");
        genre.add("132");
        playlistDto.setGenres(genre);
        int duration = 6000;
        int genreTime = (int) (duration * (Float.parseFloat(time.get(0)) / 100.0f));
        int genreTime2 = (int) (duration * (Float.parseFloat(time.get(1)) / 100.0f));

        Track track = Factory.createTack();

        List<Track> newTracks = new ArrayList<>();
        newTracks.add(track);
        Mockito.when(tracksService.trackList("Metal",genreTime,true,true)).thenReturn(newTracks);
        Mockito.when(tracksService.trackList("Pop",genreTime2,true,true)).thenReturn(newTracks);
//        Mockito.when(tracksService.trackList("152",duration)).thenReturn(newTracks);
//        Mockito.when(tracksService.trackList("132",duration)).thenReturn(newTracks);
//        Mockito.when(genresRepository.findById(152)).thenReturn(newGenre);
        Mockito.when(genresRepository.findByIdName(152)).thenReturn("Metal");
        Mockito.when(genresRepository.findByIdName(132)).thenReturn("Pop");
//        Mockito.when(tracksRepository.findtracks("Metal")).thenReturn(newTracks);
//        Mockito.when(tracksRepository.findtracks("Pop")).thenReturn(newTracks);
        Mockito.when(dtoMapper.playlistFromDto(playlistDto)).thenReturn(playlist);
//        Mockito.when(tracksService.trackList(genresRepository.findByIdName(152),duration)).thenReturn(newTracks);
//        Mockito.when(tracksService.trackList(genresRepository.findByIdName(152),duration)).thenReturn(newTracks);

        playlist.setPlaylist(newTracks);
        //Act

        playlistsService.createNewPlaylist(playlistDto,duration,true,true);
        //Assert
        Mockito.verify(playlistsRepository,Mockito.times(1)).save(playlist);
    }




}


