const myBtn = document.getElementById('mybtn');

const fillHref = function () {
    let mySelect = document.getElementById("mySelect");
    let selectedValue = mySelect.options[mySelect.selectedIndex].value;

    myBtn.setAttribute("href", `/genre/post/${selectedValue}`);
    console.log(myBtn.getAttribute('href'));
};


myBtn.addEventListener('mouseover', fillHref);