package com.telerikacademy.ridepal.controllers;

import com.telerikacademy.ridepal.exceptions.DuplicateEntityException;
import com.telerikacademy.ridepal.models.DtoMapper;
import com.telerikacademy.ridepal.models.User;
import com.telerikacademy.ridepal.models.UserDetails;
import com.telerikacademy.ridepal.models.UserDto;
import com.telerikacademy.ridepal.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Base64;
import java.util.List;

@Controller
public class RegistrationController {
    private static final String PASSWORD_DOES_NOT_MATCH = "Password does't match!";
    private static final String CAN_NOT_BE_EMPTY = "Username/password/email can't be empty!";
    private static final String USER_WITH_THE_SAME_USERNAME_ALREADY_EXISTS = "User with the same username already exists!";
    private static final String EMAIL_ALREADY_REGISTERED = "This e-mail has been already registered.";
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;
    private UsersService usersService;
    private DtoMapper dtoMapper;

    @Autowired
    public RegistrationController(UserDetailsManager userDetailsManager, PasswordEncoder passwordEncoder, UsersService usersService, DtoMapper dtoMapper) {
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.usersService = usersService;
        this.dtoMapper = dtoMapper;
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("userDto", new UserDto());
        return "register";
    }

    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute UserDto userDto, BindingResult bindingResult, Model model, @RequestParam("file") MultipartFile file) throws IOException {

        if (!userDto.getPassword().equals(userDto.getPasswordConfirmation())) {
            model.addAttribute("error", PASSWORD_DOES_NOT_MATCH);
            return "register";
        }

        if (bindingResult.hasErrors()) {

            model.addAttribute("error", CAN_NOT_BE_EMPTY);
            return "register";
        }

        if (userDetailsManager.userExists(userDto.getName())) {
            model.addAttribute("error", USER_WITH_THE_SAME_USERNAME_ALREADY_EXISTS);
            return "register";
        }

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        userDto.getName(),
                        passwordEncoder.encode(userDto.getPassword()),
                        authorities);
        try {
            UserDetails user = dtoMapper.userFromDto(userDto);
            user.setImage(Base64.getEncoder().encodeToString(file.getBytes()));
            usersService.create(user);
        } catch (DuplicateEntityException de){
            model.addAttribute("error", EMAIL_ALREADY_REGISTERED);
            return "register";
        }
        userDetailsManager.createUser(newUser);

        return "register-confirmation";
    }
    @GetMapping("/delete-confirmation")
    public String showDeleteConfirmation() {
        return "delete-confirmation";
    }

    @GetMapping("/register-confirmation")
    public String showRegisterConfirmation() {
        return "register-confirmation";
    }


}