package com.telerikacademy.ridepal.controllers;

import com.telerikacademy.ridepal.exceptions.DuplicateEntityException;
import com.telerikacademy.ridepal.exceptions.EntityNotFoundException;
import com.telerikacademy.ridepal.exceptions.ShortJourneyException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(EntityNotFoundException.class)
    public String handleIdError(EntityNotFoundException e){
        return "error-404.html";
    }

    @ExceptionHandler(DuplicateEntityException.class)
    public String handleDuplicateError(DuplicateEntityException e){
        return "duplicate.html";
    }

    @ExceptionHandler(ShortJourneyException.class)
    public String handleShortJourney(ShortJourneyException e){
        return "noPlaylist.html";
    }
}
