package com.telerikacademy.ridepal.controllers.rest;

import com.telerikacademy.ridepal.exceptions.EntityNotFoundException;
import com.telerikacademy.ridepal.models.Track;
import com.telerikacademy.ridepal.services.TracksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/tracks")
public class TracksRestController {

    private TracksService tracksService;

    @Autowired
    public TracksRestController(TracksService tracksService) {
        this.tracksService = tracksService;
    }

    @GetMapping
    public List<Track> getAll() {

        return tracksService.getAll();
    }

    @GetMapping("/{id}")
    public Track getById(@PathVariable int id) {
        try {
            return tracksService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
