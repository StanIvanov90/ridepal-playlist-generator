package com.telerikacademy.ridepal.controllers.rest;

import com.telerikacademy.ridepal.exceptions.EntityNotFoundException;
import com.telerikacademy.ridepal.models.Artist;
import com.telerikacademy.ridepal.services.ArtistsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/artists")
public class ArtistsRestController {

    private ArtistsService artistsService;

    @Autowired
    public ArtistsRestController(ArtistsService artistsService) {
        this.artistsService = artistsService;
    }
    @GetMapping
    public List<Artist> getAll() {

        return artistsService.getAll();
    }

    @GetMapping("/{id}")
    public Artist getById(@PathVariable int id) {
        try {
            return artistsService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}
