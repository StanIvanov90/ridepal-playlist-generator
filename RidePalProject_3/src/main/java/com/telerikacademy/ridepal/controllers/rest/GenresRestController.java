package com.telerikacademy.ridepal.controllers.rest;

import com.telerikacademy.ridepal.exceptions.EntityNotFoundException;
import com.telerikacademy.ridepal.models.Genre;
import com.telerikacademy.ridepal.services.GenresService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/genres")
public class GenresRestController {

    private GenresService genresService;

    @Autowired
    public GenresRestController(GenresService genresService) {
        this.genresService = genresService;
    }

    @GetMapping
    public List<Genre> getAll() {

        return genresService.getAll();
    }

    @GetMapping("/{id}")
    public Genre getById(@PathVariable int id) {
        try {
            return genresService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
