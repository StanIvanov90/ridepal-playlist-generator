package com.telerikacademy.ridepal.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.telerikacademy.ridepal.helper.DBHelper;
import com.telerikacademy.ridepal.exceptions.EntityNotFoundException;
import com.telerikacademy.ridepal.models.Album;
import com.telerikacademy.ridepal.repositories.AlbumsRepository;
import com.telerikacademy.ridepal.repositories.ArtistsRepository;
import com.telerikacademy.ridepal.repositories.GenresRepository;
import com.telerikacademy.ridepal.repositories.TracksRepository;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class AlbumsService {
    private static final String ALBUM_WITH_ID_DOES_NOT_EXIST = "Album with id %d doesn't exist.";
    private ArtistsRepository artistsRepository;
    private AlbumsRepository albumsRepository;
    private TracksRepository trackRepository;
    private GenresRepository genresRepository;
    private ObjectMapper objectMapper;
    private RestTemplate restTemplate;
    private DBHelper dbHelper;

    @Autowired
    public AlbumsService(TracksRepository trackRepository, ObjectMapper objectMapper, RestTemplate restTemplate,
                         GenresRepository genresRepository, AlbumsRepository albumsRepository,
                         ArtistsRepository artistsRepository, JSONParser jsonParser) {
        this.trackRepository = trackRepository;
        this.objectMapper = objectMapper;
        this.restTemplate = restTemplate;
        this.genresRepository = genresRepository;
        this.albumsRepository = albumsRepository;
        this.artistsRepository = artistsRepository;
        this.dbHelper = new DBHelper(artistsRepository,
                albumsRepository,
                genresRepository,
                trackRepository,
                objectMapper,
                restTemplate,
                jsonParser);
    }

    public List<Album> getAll() {
        return albumsRepository.getAll();
    }

    public Album getById(int id) {
        if (!albumsRepository.existsById(id)) {
            throw new EntityNotFoundException(String.format(ALBUM_WITH_ID_DOES_NOT_EXIST, id));
        }
        return albumsRepository.getAlbumById(id);
    }

    public void populateArtistsAlbumsByGenre(String name) throws ParseException, JsonProcessingException, InterruptedException {
        dbHelper.populateArtistsAlbumsByGenre(name);
    }

}

