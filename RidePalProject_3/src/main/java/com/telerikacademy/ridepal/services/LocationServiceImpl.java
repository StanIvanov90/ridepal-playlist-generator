package com.telerikacademy.ridepal.services;

import com.telerikacademy.ridepal.helper.ApiHelper;
import com.telerikacademy.ridepal.models.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LocationServiceImpl implements LocationService{

    private ApiHelper apiHelper;

    @Autowired
    public LocationServiceImpl(ApiHelper apiHelper) {
        this.apiHelper = apiHelper;
    }


    @Override
    public Location getLocationParameters(String start, String end) {
        return apiHelper.getLocationParameters(start,end);
    }

}
