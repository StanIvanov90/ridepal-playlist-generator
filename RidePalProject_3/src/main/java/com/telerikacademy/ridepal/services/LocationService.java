package com.telerikacademy.ridepal.services;

import com.telerikacademy.ridepal.models.Location;

public interface LocationService {

    Location getLocationParameters(String start, String end);

}
