package com.telerikacademy.ridepal.models;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "roles")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="role_id")
    private int id;

    @Column(name = "role")
    private String role;

    @ManyToMany(mappedBy = "roles")
    private List<UserDetails> users;

    public Role(){

    }

    public String getRole() {
        return role;
    }

    public List<UserDetails> getUsers() {
        return new ArrayList<>(this.users);
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setUsers(List<UserDetails> users) {
        this.users = users;
    }
}
