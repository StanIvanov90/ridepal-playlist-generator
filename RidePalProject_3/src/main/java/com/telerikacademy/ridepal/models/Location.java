package com.telerikacademy.ridepal.models;

import static com.telerikacademy.ridepal.helper.TimeHelper.getTimeDuration;

public class Location {

    private String startLocationName;
    private String endLocationName;
    private double startAbscissa;
    private double startOrdinate;
    private double endAbscissa;
    private double endOrdinate;
    private double avgAbscissa;
    private double avgOrdinate;
    private double duration;


    public Location() {
    }

    public double getAvgX() {
        return avgAbscissa;
    }

    public void setAvgX(double avgAbscissa) {
        this.avgAbscissa = avgAbscissa;
    }

    public double getAvgY() {
        return avgOrdinate;
    }

    public void setAvgY(double avgOrdinate) {
        this.avgOrdinate = avgOrdinate;
    }

    public double getDuration() {

        return duration;
    }

    public String getDurationUser() {
        return getTimeDuration(duration);
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public double getStartX() {
        return startAbscissa;
    }

    public void setStartX(double startAbscissa) {
        this.startAbscissa = startAbscissa;
    }

    public double getStartY() {
        return startOrdinate;
    }

    public void setStartY(double startOrdinate) {
        this.startOrdinate = startOrdinate;
    }

    public double getEndX() {
        return endAbscissa;
    }

    public void setEndX(double endAbscissa) {
        this.endAbscissa = endAbscissa;
    }

    public double getEndY() {
        return endOrdinate;
    }

    public void setEndY(double endOrdinate) {
        this.endOrdinate = endOrdinate;
    }

    public String getStart() {
        return startLocationName;
    }

    public void setStart(String startLocationName) {
        this.startLocationName = startLocationName;
    }

    public String getEnd() {
        return endLocationName;
    }

    public void setEnd(String endLocationName) {
        this.endLocationName = endLocationName;
    }

}
