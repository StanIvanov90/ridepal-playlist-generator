package com.telerikacademy.ridepal.models;

import java.util.List;

public class PlaylistDto {

    private String title;
    private String description;
    private int duration;
    private int rank;
    private UserDetails userDetails;
    private String cover;
    private List<String> time;
    private List<String> genres;
    private String startLocation;
    private String endLocation;
    private boolean topTracks;
    private boolean useArtists;
    private int id;

    public PlaylistDto() { }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public UserDetails getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public List<String> getTime() {
        return time;
    }

    public void setTime(List<String> time) {
        this.time = time;
    }

    public List<String> getGenres() {
        return genres;
    }

    public void setGenres(List<String> genres) {
        this.genres = genres;
    }

    public String getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(String startLocation) {
        this.startLocation = startLocation;
    }

    public String getEndLocation() {
        return endLocation;
    }

    public void setEndLocation(String endLocation) {
        this.endLocation = endLocation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isTopTracks() {
        return topTracks;
    }

    public void setTopTracks(boolean topTracks) {
        this.topTracks = topTracks;
    }

    public boolean isUseArtists() {
        return useArtists;
    }

    public void setUseArtists(boolean useArtists) {
        this.useArtists = useArtists;
    }
}

