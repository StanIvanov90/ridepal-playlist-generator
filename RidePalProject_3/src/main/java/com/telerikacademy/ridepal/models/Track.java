package com.telerikacademy.ridepal.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.telerikacademy.ridepal.models.Album;
import com.telerikacademy.ridepal.models.Artist;

import javax.persistence.*;

import static com.telerikacademy.ridepal.helper.TimeHelper.getTimeDuration;

@Entity
@Table(name = "tracks")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Track {
    @Id
    @Column(name = "track_id")
    private long id;
    @Column(name = "title")
    private String title;
    @Column(name = "duration")
    private String duration;
    @Column(name = "rank")
    private String rank;
    @Column(name = "preview")
    private String preview;
    @Column(name = "enabled")
    private boolean enabled;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "artist_id")
    private Artist artist;

    @ManyToOne
    @JoinColumn(name = "album_id")
    private Album album;

    public Track() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }

    public String getDurationUser() {
        return getTimeDuration(Double.parseDouble(duration));
    }

    @Override
    public String toString() {
        return String.format("%s",preview);
    }
}

