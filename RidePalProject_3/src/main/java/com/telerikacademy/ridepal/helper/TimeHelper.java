package com.telerikacademy.ridepal.helper;

public class TimeHelper {
    public static String getTimeDuration(double duration) {
        double min = duration / 60.0;
        String numberD = String.valueOf(min);
        numberD = numberD.substring(numberD.indexOf("."));
        numberD = numberD.replace(".", "0.");
        double sec = 60 * Double.parseDouble(numberD);
        double hours = min / 60.0;
        String numberM = String.valueOf(hours);
        numberM = numberM.substring(numberM.indexOf("."));
        numberM = numberM.replace(".", "0.");
        min = 60 * Double.parseDouble(numberM);
        String fullHours = Integer.toString((int) hours);
        String minutes = Integer.toString((int) min);
        String seconds = Integer.toString((int) sec);

        if (fullHours.length() == 1) {
            fullHours = "0" + fullHours;
        }
        if (minutes.length() == 1) {
            minutes = "0" + minutes;
        }
        if (seconds.length() == 1) {
            seconds = "0" + seconds;
        }
        return (String.format("%s:%s:%s", fullHours, minutes, seconds));
    }
}
