package com.telerikacademy.ridepal.helper;

import com.telerikacademy.ridepal.models.Location;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Random;

import static com.telerikacademy.ridepal.constants.CommandConstants.API_MAPS_URL;
import static com.telerikacademy.ridepal.constants.CommandConstants.API_PICTURE_URL;

@Component
public class ApiHelper {
    private  RestTemplate restTemplate;

    @Autowired
    public ApiHelper(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public Location getLocationParameters(String start, String end) {
        JSONObject jsonObject = new JSONObject(getHTML(start,end));
        Location location = new Location();
        location.setDuration(getTravelDuration(jsonObject));
        location.setStart(start);
        location.setEnd(end);
        location.setStartX(getGeographicalCoordinatesForStartPointAbscissa(jsonObject));
        location.setStartY(getGeographicalCoordinatesForStartPointOrdinate(jsonObject));
        location.setEndX(getGeographicalCoordinatesForEndPointAbscissa(jsonObject));
        location.setEndY(getGeographicalCoordinatesForEndPointOrdinate(jsonObject));
        return location;
    }
    public String getPictureForPlaylist(){
        JSONObject jsonObject = new JSONObject(getPictureFromHTML());
        String picture = getPicture(jsonObject);
        return picture;
    }

    private  String getPictureFromHTML(){
        String html = "https://pixabay.com/api/?key=15101104-87b2b8ea4ca5efca962970660&q=music";
        return restTemplate.getForObject(html, String.class);
    }

    private String getPicture(JSONObject jsonObject) {
        Random r = new Random();
        int randomInt = r.nextInt(20);
        return jsonObject.getJSONArray("hits")
                .getJSONObject(randomInt)
                .getString("webformatURL");
    }


    private String getHTML(String start, String end){
        String html = String.format(API_MAPS_URL, start, end);
        return restTemplate.getForObject(html, String.class);
    }

    private Double getTravelDuration(JSONObject jsonObject) {
        return jsonObject.getJSONArray("resourceSets")
                .getJSONObject(0)
                .getJSONArray("resources")
                .getJSONObject(0).getDouble("travelDuration");
    }

    private Double getGeographicalCoordinatesForStartPointAbscissa(JSONObject jsonObject) {
        return Double.parseDouble(jsonObject
                .getJSONArray("resourceSets")
                .getJSONObject(0)
                .getJSONArray("resources")
                .getJSONObject(0)
                .getJSONArray("routeLegs")
                .getJSONObject(0)
                .getJSONObject("actualStart")
                .getJSONArray("coordinates")
                .toString()
                .replace("]", "")
                .replace("[", "")
                .split(",")[0]);
    }

    private Double getGeographicalCoordinatesForStartPointOrdinate(JSONObject jsonObject) {
        return Double.parseDouble(jsonObject
                .getJSONArray("resourceSets")
                .getJSONObject(0)
                .getJSONArray("resources")
                .getJSONObject(0)
                .getJSONArray("routeLegs")
                .getJSONObject(0)
                .getJSONObject("actualStart")
                .getJSONArray("coordinates")
                .toString()
                .replace("]", "")
                .replace("[", "")
                .split(",")[1]);
    }

    private Double getGeographicalCoordinatesForEndPointAbscissa(JSONObject jsonObject) {
        return Double.parseDouble(jsonObject
                .getJSONArray("resourceSets")
                .getJSONObject(0)
                .getJSONArray("resources")
                .getJSONObject(0)
                .getJSONArray("routeLegs")
                .getJSONObject(0)
                .getJSONObject("actualEnd")
                .getJSONArray("coordinates")
                .toString()
                .replace("]", "")
                .replace("[", "")
                .split(",")[0]);
    }

    private Double getGeographicalCoordinatesForEndPointOrdinate(JSONObject jsonObject) {
        return Double.parseDouble(jsonObject
                .getJSONArray("resourceSets")
                .getJSONObject(0)
                .getJSONArray("resources")
                .getJSONObject(0)
                .getJSONArray("routeLegs")
                .getJSONObject(0)
                .getJSONObject("actualEnd")
                .getJSONArray("coordinates")
                .toString()
                .replace("]", "")
                .replace("[", "")
                .split(",")[1]);
    }
}
