package com.telerikacademy.ridepal.helper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.telerikacademy.ridepal.models.Album;
import com.telerikacademy.ridepal.models.Artist;
import com.telerikacademy.ridepal.models.Genre;
import com.telerikacademy.ridepal.models.Track;
import com.telerikacademy.ridepal.repositories.AlbumsRepository;
import com.telerikacademy.ridepal.repositories.ArtistsRepository;
import com.telerikacademy.ridepal.repositories.GenresRepository;
import com.telerikacademy.ridepal.repositories.TracksRepository;
import com.telerikacademy.ridepal.services.UsersServiceImpl;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;
import org.slf4j.Logger;

import java.util.List;
import java.util.Objects;

import static com.telerikacademy.ridepal.constants.CommandConstants.ALBUM_URL;
import static com.telerikacademy.ridepal.constants.CommandConstants.GENRE_URL;
import static com.telerikacademy.ridepal.constants.CommandConstants.GENRE_ARTISTS_URL;
import static com.telerikacademy.ridepal.constants.CommandConstants.ARTIST_URL;
import static com.telerikacademy.ridepal.constants.CommandConstants.APPEND_ARTISTS;
import static com.telerikacademy.ridepal.constants.CommandConstants.GET_ARTIST_ID;
import static com.telerikacademy.ridepal.constants.CommandConstants.GET_JSON_ARRAY_FROM_JSON_OBJECT;

public class DBHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsersServiceImpl.class);

    private ArtistsRepository artistsRepository;
    private AlbumsRepository albumsRepository;
    private GenresRepository genresRepository;
    private TracksRepository trackRepository;
    private ObjectMapper objectMapper;
    private RestTemplate restTemplate;
    private JSONParser jsonParser;

    public DBHelper(ArtistsRepository artistsRepository, AlbumsRepository albumsRepository,
                    GenresRepository genresRepository, TracksRepository trackRepository, ObjectMapper objectMapper,
                    RestTemplate restTemplate, JSONParser jsonParser) {
        this.artistsRepository = artistsRepository;
        this.albumsRepository = albumsRepository;
        this.genresRepository = genresRepository;
        this.trackRepository = trackRepository;
        this.objectMapper = objectMapper;
        this.restTemplate = restTemplate;
        this.jsonParser = jsonParser;
    }

    public void populateGenres() throws JsonProcessingException, ParseException {
        String html = GENRE_URL;
        String response = restTemplate.getForObject(
                html,
                String.class);
        JSONObject jsonObject = (JSONObject) jsonParser.parse(response);
        JSONArray jsonArray = (JSONArray) jsonObject.get(GET_JSON_ARRAY_FROM_JSON_OBJECT);
        String track = objectMapper.writeValueAsString(jsonArray);

        List<Genre> result = objectMapper.readValue(track, new TypeReference<List<Genre>>() {
        });

        // Save the list into a database
        if (Objects.nonNull(result))
            result.stream().filter(Objects::nonNull).forEach(element -> genresRepository.saveAndFlush(element));
    }


    public JSONArray getArtistsByGenre(String name) throws ParseException, JsonProcessingException {
        Genre genre = genresRepository.findByName(name);
        StringBuilder sb = new StringBuilder();
        sb.append(GENRE_ARTISTS_URL);
        sb.append(genre.getId());
        sb.append(APPEND_ARTISTS);
        String response = restTemplate.getForObject(
                sb.toString(),
                String.class);
        JSONObject jsonObject = (JSONObject) jsonParser.parse(response);
        JSONArray jsonArray = (JSONArray) jsonObject.get(GET_JSON_ARRAY_FROM_JSON_OBJECT);
        return jsonArray;
    }

    public void populateArtistsByGenre(String name) throws JsonProcessingException, ParseException {
        String artists = objectMapper.writeValueAsString(getArtistsByGenre(name));
        List<Artist> result = objectMapper.readValue(artists, new TypeReference<List<Artist>>() {
        });
        // Save the list into a database
        if (Objects.nonNull(result))
            result.stream().filter(Objects::nonNull).forEach(element -> artistsRepository.saveAndFlush(element));
        LOGGER.info("All Artists from " + name + " genre were added.");
    }

    public void populateArtistsAlbumsByGenre(String name) throws ParseException, JsonProcessingException, InterruptedException {
        JSONArray jsonArray = getArtistsByGenre(name);
        JSONParser jsonParser = new JSONParser();
        for (Object object : jsonArray) {
            JSONObject record = (JSONObject) object;
            int id = Integer.parseInt(String.valueOf(record.get(GET_ARTIST_ID)));
            StringBuilder sb = new StringBuilder();
            sb.append(ARTIST_URL);
            sb.append(id);
            sb.append(ALBUM_URL);
            String albumsResponse = restTemplate.getForObject(
                    sb.toString(),
                    String.class);
            JSONObject jsonObjectAlbums = (JSONObject) jsonParser.parse(albumsResponse);
            JSONArray jsonAlbumsArray = (JSONArray) jsonObjectAlbums.get(GET_JSON_ARRAY_FROM_JSON_OBJECT);
            String track = objectMapper.writeValueAsString(jsonAlbumsArray);
            List<Album> result = objectMapper.readValue(track, new TypeReference<List<Album>>() {
            });
            // Save the list into a database
            if (Objects.nonNull(result))
                result.stream().filter(Objects::nonNull).forEach(element -> albumsRepository.saveAndFlush(element));
        }
        LOGGER.info("Albums from " + name + " genre were added.");
    }

    public void populateSongsByGenre(String name) throws ParseException, JsonProcessingException, InterruptedException {
        LOGGER.info("Adding tracks from " + name + " genre has started.");
        List<Album> albums = albumsRepository.findAlbumsByGenre(name);
        JSONParser jsonParser = new JSONParser();
        for (Album album : albums) {
//            JSONObject record = (JSONObject) object;
//            String url = (String) record.get("tracklist");
            String url = album.getTracklist();
            if(url.isEmpty()){
                continue;
            }
            Thread.sleep(1000);
            String response1 = restTemplate.getForObject(
                    url,
                    String.class);
            JSONObject jsonObject1 = (JSONObject) jsonParser.parse(response1);
            JSONArray jsonArray1 = (JSONArray) jsonObject1.get(GET_JSON_ARRAY_FROM_JSON_OBJECT);
            String track = objectMapper.writeValueAsString(jsonArray1);
            List<Track> result = objectMapper.readValue(track, new TypeReference<List<Track>>() {
            });
            for (Track track1: result) {
                track1.setAlbum(albumsRepository.getOne(album.getId()));
            }
            result.forEach(element-> element.setEnabled(true));
            // Save the list into a database
            if (Objects.nonNull(result))
                result.stream().filter(Objects::nonNull).forEach(element -> trackRepository.saveAndFlush(element));
        }
        LOGGER.info("Tracks from " + name + " genre were added.");
    }
}



