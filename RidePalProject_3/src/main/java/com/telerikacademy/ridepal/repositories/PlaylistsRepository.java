package com.telerikacademy.ridepal.repositories;

import com.telerikacademy.ridepal.models.Playlist;
import com.telerikacademy.ridepal.models.Track;
import com.telerikacademy.ridepal.models.UserDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

@Repository
public interface PlaylistsRepository extends JpaRepository<Playlist, Serializable> {

    @Query(value = "select *\n" +
            "from playlists\n" +
            "where enabled=1\n" +
            "order by `rank` DESC ;\n", nativeQuery = true)
    List<Playlist> getAll();


    @Query(value = "select *\n" +
            "from playlists\n" +
            "where enabled=1 and user_id=?1\n" +
            "order by `rank` DESC ;\n", nativeQuery = true)
    List<Playlist> getPlaylistsByUser(int id);


    @Query(value = "select *\n" +
            "from playlists\n" +
            "where enabled=1 and duration > ?1 and duration <?2", nativeQuery = true)
    List<Playlist> filterByDuration(int startDuration, int endDuration, List<Playlist> result);

    @Query(value = "select *\n" +
            "from playlists\n" +
            //            "from playlists\n" +
            "JOIN playlist_genres pg on playlists.playlist_id = pg.playlist_id\n" +
            "join genres g on pg.genre_id = g.genre_id\n" +
            "where playlists.enabled=1 and genre_name= ?1 and user_id =?2", nativeQuery = true)
    List<Playlist> filterByGenreAndUser(String genre, int user_id, List<Playlist> result);

    @Query(value = "select *\n" +
            "from playlists\n" +
            "where enabled=1 and duration > ?1 and duration <?2 and  user_id=?3", nativeQuery = true)
    List<Playlist> filterByDurationAndUserDetails(int startDuration, int endDuration, int user_id,List<Playlist> result);
//    @Query(value = "select *\n" +
//            "from playlists\n" +
//            "where enabled=1 and title like ?1", nativeQuery = true)
//    List<Playlist> filterByName(String name, List<Playlist> result);

    List<Playlist> findAllByTitleContainingAndEnabledTrue(String name);

    List<Playlist> findAllByTitleContainingAndEnabledTrueAndAndUserDetails(String name, UserDetails user);

//    List<Playlist> findAllByGenresExistsAndUserDetails(String name, UserDetails user);
//    @Query(value = "select *\n" +
//            "from playlists\n" +
//            "JOIN playlist_genres pg on playlists.playlist_id = pg.playlist_id\n" +
//            "join genres g on pg.genre_id = g.genre_id\n" +
//            "where enabled=1 and genre_name = ?1", nativeQuery = true)
//    List<Playlist> filterByGenre(String genre, List<Playlist> result);

    @Query("SELECT p from Playlist p Left JOIN p.genres as pg where pg.name like %:name% and p.enabled=1")
    List<Playlist> filterByGenre(String name);

//    @Query("SELECT p from Playlist p Left JOIN p.genres as pg where pg.name like %:name% and p.enabled=1")
//    List<Playlist> filterByGenreAndUserDetails(String name, UserDetails userDetails);

    @Query(value = "select *\n" +
            "from playlists\n" +
            "where enabled=1 and playlist_id=?1", nativeQuery = true)
    Playlist getById(int id);


}

