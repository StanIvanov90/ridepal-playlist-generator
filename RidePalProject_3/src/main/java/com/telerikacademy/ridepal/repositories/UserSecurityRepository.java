package com.telerikacademy.ridepal.repositories;

import com.telerikacademy.ridepal.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository
public interface UserSecurityRepository extends JpaRepository<User, Serializable> {

    User getByUsername(String name);
}
