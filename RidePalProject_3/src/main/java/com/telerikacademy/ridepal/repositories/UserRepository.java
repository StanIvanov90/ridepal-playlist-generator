package com.telerikacademy.ridepal.repositories;

import com.telerikacademy.ridepal.models.User;
import com.telerikacademy.ridepal.models.UserDetails;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


public interface UserRepository {
    UserDetails getByUsername(String username);

    List<UserDetails> getAll();

    UserDetails getById(int id);

    void create(UserDetails userDetails);

    void delete(int id);

    void update(int id, UserDetails user);

    boolean userExists(int id);

    boolean isDeleted(String username);

    boolean checkEmailExists(String email);

    boolean checkUsernameExists(String username);

    int getUserID(String username);
}
