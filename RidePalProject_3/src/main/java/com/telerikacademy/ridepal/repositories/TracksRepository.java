package com.telerikacademy.ridepal.repositories;

import com.telerikacademy.ridepal.models.Track;
import org.hibernate.SessionFactory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

@Repository
public interface TracksRepository extends JpaRepository<Track, Serializable> {

    @Query(value = "select *\n" +
            "from tracks t join albums a on t.album_id = a.album_id\n" +
            "join genres g on a.genre_id = g.genre_id\n" +
            "where genre_name = ?1 and t.duration <= 300 and t.title not like '%??%' order by RAND()", nativeQuery = true)
    List<Track> findtracks(String name);

    @Query(value = "select *\n" +
            "from tracks t join albums a on t.album_id = a.album_id\n" +
            "join genres g on a.genre_id = g.genre_id\n" +
            "where genre_name = ?1 and t.duration <= 300 and t.title not like '%??%' GROUP BY artist_id order by RAND()", nativeQuery = true)
    List<Track> findtracksDistinctArtist(String name);

    @Query(value = "select *\n" +
            "from tracks t join albums a on t.album_id = a.album_id\n" +
            "join genres g on a.genre_id = g.genre_id\n" +
            "where genre_name = ?1 and t.duration <= 300 and t.title not like '%??%' GROUP BY artist_id order by `rank` desc", nativeQuery = true)
    List<Track> findTopByRankDistinct(String name);


    @Query(value = "select *\n" +
            "from tracks t join albums a on t.album_id = a.album_id\n" +
            "join genres g on a.genre_id = g.genre_id\n" +
            "where genre_name = ?1 and t.duration <= 300 and t.title not like '%??%' order by `rank` desc", nativeQuery = true)
    List<Track> findTop(String name);

    @Query(value = " select *\n" +
            "    from tracks\n" +
            "    join playlist_tracks pt on tracks.track_id = pt.track_id\n" +
            "    join playlists p on pt.playlist_id = p.playlist_id\n" +
            "    where p.playlist_id=?1 and tracks.title not like '%??%'", nativeQuery = true)
    List<Track> getTracksByPlaylistId(int id);

    @Query("from Track ")
    List<Track> getAll();

    Track getTrackById(int id);
}
