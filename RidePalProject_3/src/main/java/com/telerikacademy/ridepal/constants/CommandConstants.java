package com.telerikacademy.ridepal.constants;


public class CommandConstants {
    public static final String USER_NOT_FOUND = "User with id: %s was not found.";
    public static final String EMAIL_ALREADY_EXISTS = "User with email %s already exists";
    public static final String USERNAME_ALREADY_EXISTS = "User with name %s already exists";
    public static final String BING_MAPS_URL = "https://www.bing.com/api/maps/mapcontrol?key=Ag3UQD6ytDHjm3IXLMwZGc2pRLphrX-lWR4rBYm46gfD-fikYQKQs87ArOgujf3P";
    public static final String API_MAPS_URL = "http://dev.virtualearth.net/REST/v1/Routes/driving?wayPoint.1=%s&wayPoint.2=%s&distanceUnit=km&key=Ag3UQD6ytDHjm3IXLMwZGc2pRLphrX-lWR4rBYm46gfD-fikYQKQs87ArOgujf3P";
    public static final String API_PICTURE_URL = "https://pixabay.com/api/?key=" + System.getenv("PictureKey") + "&q=music";
    public static final String PLAYLIST_NOT_FOUND = "Playlists with name %s not found";
    public static final String PLAYLIST_WITH_ID_NOT_FOUND = "There is no playlist with this id.";
    public static final String PLAYLIST_WITH_ID_DOES_NOT_EXIST = "Playlist with this %d doesn't exist";
    public static final String PLAYLISTS_WITH_GENRE_NOT_FOUND = "Playlists with genre %s not found";
    public static final String PLAYLIST_WAS_DELETED = "Playlist was deleted!";
    public static final String USER_WAS_DELETED = "User was deleted!";
    public static final String ALL_GENRES_INFO_DOWNLOADED = "Genres info was downloaded!";
    public static final String GENRE_SYNCHRONIZED = "Genre was synchronized!";
    public static final String GENRE_WITH_ID_DOES_NOT_EXIST = "Genre with id %d doesn't exist.";
    public static final String GENRE_NOT_FOUND = "Genre with %s name not found.!";
    public static final int FIVE_MINUTES = 300;
    public static final String ALBUM_URL = "/albums&limit=5";
    public static final String GENRE_URL = "https://api.deezer.com/genre";
    public static final String GENRE_ARTISTS_URL = "https://api.deezer.com/genre/";
    public static final String ARTIST_URL = "https://api.deezer.com/artist/";
    public static final String APPEND_ARTISTS = "/artists";
    public static final String GET_ARTIST_ID = "id";
    public static final String GET_JSON_ARRAY_FROM_JSON_OBJECT = "data";
    public static final String SHORT_JOURNEY = "Journey is too short to create playlist.";
}